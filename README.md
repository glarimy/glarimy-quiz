# Environment Setup guide

1. Install node.js in terminal.
2. Install the dependencies using "npm i" command.
3. Start the Express server by using "npm start" command.
4. Proceed to make API calls.

CURL commands to be used make API calls

Register API

- curl -X POST -H "Content-Type: application/json" -d '{"email":"xxx@xxx.xxx", "password": "xxxxxxxx"}' http://localhost:8080/quiz/register

Login API

- curl -X POST -H "Content-Type: application/json" -d '{"email":"xxx@xxx.xxx", "password": "xxxxxxxx"}' http://localhost:8080/quiz/login

Logout API

- curl -X POST -H "Content-Type: application/json" -d '{"email":"xxx@xxx.xxx", "password": "xxxxxxxx"}' http://localhost:8080/quiz/logout

# Test Cases for the committed code

Test case 01:

Details: In this scenarion we are testing for register as an user without mentioning email key or value(empty json body).

Input data: curl -X POST -H "Content-Type: application/json" -d '{ }' http://localhost:8080/quiz/register

Expected output(negative) : In response { "msg": "please enter valid email" }

Test case 02:

Details: In this scenarion we are testing for register as an user with improper email value.

Input data: curl -X POST -H "Content-Type: application/json" -d '{"email":"xxx" }' http://localhost:8080/quiz/register

Expected output(negative) : In response { "msg": "please enter valid email" }

Test case 03:

Details: In this scenarion we are testing for register as an user with proper email value i.e:xxx@xxx.xxx format .

Input data: curl -X POST -H "Content-Type: application/json" -d '{"email":"xxx@xxx.xxx"}' http://localhost:8080/quiz/register

Expected output(positive, meaning email validated and password yet to be validated) : In response { "msg": "please enter valid password" }

Test case 04:

Details: In this scenarion we are testing for register as an user with improper password value.

Input data: curl -X POST -H "Content-Type: application/json" -d '{"email":"xxx@xxx.xxx", "password": "xxx"}' http://localhost:8080/quiz/register

Expected output(negative) : In response { "msg": "please enter valid password" }

Test case 05:

Details: In this scenarion we are testing for register as an user with proper password value i.e:atleast one small alphabet and one digit and minimum 8 not exceeding 16.

Input data: curl -X POST -H "Content-Type: application/json" -d '{"email":"xxx@xxx.xxx", "password": "xxxxxxxx"}' http://localhost:8080/quiz/register

Expected output(positive) : In response 201 status is shown.

Test case 06:

Details: In this scenarion we are testing for register as an user with an already registered email.

Input data: curl -X POST -H "Content-Type: application/json" -d '{"email":"xxx@xxx.xxx", "password": "xxxxxxxx"}' http://localhost:8080/quiz/register

Expected output(positive) : In response { msg: "user exists" }.
