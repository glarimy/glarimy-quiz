const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const config = require("../config");
const uri = config.MONGODB.uri;
const UsersCollection = config.MONGODB.UsersCollection;
const liveServer = config.SERVER;
const port = config.PORT;
const jwtSecret = config.JWT.secret;
const transporter = nodemailer.createTransport({
  service: "yahoo",
  auth: { user: config.MAIL.user, pass: config.MAIL.pass }
});
const from = config.MAIL.user;
const subject = config.MAIL.subject;
const text = config.MAIL.text;
const fs = require("fs");
const morgan = require("morgan");
const accessLogStream = fs.createWriteStream(__dirname + "/../logs/quiz.log", {
  flags: "a"
});
app.use(morgan("combined", { stream: accessLogStream }));

const log4js = require("log4js");
log4js.configure({
  appenders: {
    fileAppender: { type: "dateFile", filename: "logs/quiz.log" }
  },
  categories: { default: { appenders: ["fileAppender"], level: "info" } }
});
const logger = log4js.getLogger();

const swaggerUi = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.1",
    info: {
      title: "Glarimy Quiz Services",
      description: "This is a sample API for subject assesment.",
      servers: [liveServer]
    }
  },
  apis: ["src/app.js"],
  components: {
    securitySchemes: {
      bearerAuth: {
        type: "http",
        scheme: "bearer",
        bearerFormat: "JWT"
      }
    }
  },
  security: [
    {
      bearerAuth: []
    }
  ]
};
const swaggerDocument = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

mongoose
  .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => logger.info("MongoDB Connected..."))
  .catch(err => logger.error(err));
mongoose.connection.on("disconnected", function() {
  server.close(() => {
    logger.warn("MongoDB Connection Failed Initiating Turn-OFF Sequence...");
    process.exit(0);
  });
});
app.use(bodyParser.json());
app.use(cors());
const server = app.listen(port, () =>
  logger.info(`Server started on port ${port}...`)
);

//Clean unsuing modules like nodemon&&Add new modules like server permanantly up.

function validateEmail(email) {
  let emailRegex = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
  return emailRegex.test(email);
}
function validatePassword(password) {
  let passwordregex = /^(?=.*?[a-z])(?=.*?[0-9]).{8,16}$/;
  return passwordregex.test(password);
}

/**
 *@swagger
 * "/quiz/register": {
 *   "post": {
   "summary": Creates a new user.,
      "consumes":
           application/json,
      "parameters":
      { "in":  body ,
          "name": user,
          "description": The user to create.,
          "schema":{
            "type": object,
            "required":
                userName,
            "properties":{
              "userName" :  string  
              ,
              "password":{ 
                "type": string 
 }}},
      "responses": {
        "201",
          "description": Created}}
 *          }
 *         }
 */
app.post("/quiz/register", (req, res) => {
  const { email, password } = req.body;
  if (!validateEmail(email)) {
    res.status(400).json({ msg: "please enter valid email" });
  } else if (!validatePassword(password)) {
    res.status(400).json({ msg: "please enter valid password" });
  } else {
    let user = new UserDetails({ email, password });
    UserDetails.findOne({ email }).then(member => {
      if (member) {
        return res.status(400).json({ msg: "user exists" });
      } else {
        let mailOptions = {
          from: from,
          to: email,
          subject: subject,
          text: text
        };
        user.save().then(
          res.sendStatus(201),
          transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              logger.error(error);
            }
            logger.info("Email sent: " + info.response);
          })
        );
      }
    });
  }
});
/**
 *@swagger
 * "/quiz/login": {
 *   "post": {
 *     "description": "After registering, Login now to proceed",
   "parameters": [
          {
            "name": "username",
            "description": "Username to use for login.",
            "in": "formData",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "description": "User's password.",
            "in": "formData",
            "required": true,
            "type": "string"
          }
        ],
 *     "responses": {
 *       "200",
 *          "description": "Login Access",
 *          default:{
 *           "description": "Something is wrong"
 *              }
 *            }
 *          }
 * }
 */
app.post("/quiz/login", (req, res) => {
  let authHeader = req.headers.authorization;
  let auth = Buffer.from(authHeader.split(" ")[1], "base64")
    .toString()
    .split(":");
  let email = auth[0];
  let password = auth[1];
  if (!authHeader) {
    res.sendStatus(403);
    return next();
  } else {
    UserDetails.findOne({ email: email, password: password }).then(member => {
      if (member) {
        jwt.sign({ email }, jwtSecret, (err, token) => {
          if (err) throw err;
          res.status(201).json({ token });
          return next();
        });
      } else {
        res.sendStatus(403);
        return next();
      }
    });
  }
});

function access(req, res, next) {
  const token = req.header("x-auth-token");
  if (!token) res.status(401).json({ msg: "Not Authorized" });
  {
    try {
      const decoded = jwt.verify(token, jwtSecret);
      req.user = decoded;
      next();
    } catch (error) {
      res.status(400).json({ msg: "Token Expired/Invalid" });
    }
  }
}
/**
 *@swagger
 * "/quiz/:subject/:level/mcq": {
 *   "get": {
 * "parameters":[
          {
            "name": "subject",
            "description": "subject.",
            "in": "formData",
            "required": true,
            "type": "string"
          },
          {
            "name": "level",
            "description": "level.",
            "in": "formData",
            "required": true,
            "type": "string"
          }
        ],
 * "security":{
 *  -"bearerAuth": []}  ,
 *     "description": "Select Subject, Level",
 *     "responses": {
 *       "200",
 *          "description": "Preference details",
 *          default:{
 *           "description": "Something is wrong"
 *              }
 *            }
 *          }
 *         }
 */
app.get("/quiz/:subject/:level/mcq", (req, res) => {
  res.type("application/json");
  const subjectName = req.params.subject;
  const level = req.params.level;
  let limit = parseInt(req.query.limit);
  dynamicModel(subjectName)
    .find({ subject: subjectName }, { answer: 0 })
    .then(paper => {
      res.json(paper);
    });
  mongoose.deleteModel(subjectName);
});
/**
 *@swagger
 * "/quiz/answers": {
 *   "post": {
 *     "description": "After submiting test, compare answers provided by user",
 *     "responses": {
 *       "200",
 *          "description": "Calculate result",
 *          default:{
 *           "description": "Something is wrong"
 *              }
 *            }
 *          }
 *         }
 */
// app.post("/quiz/answers", access, (req, res) => {
app.post("/quiz/answers", (req, res) => {
  res.type("application/json");
  var array = req.body;
  const subjectName = req.params.subject;
  let score = 0;
  for (var i = 0; i < array.length; i++) {
    dynamicModel(subjectName)
      .findOne({ _id: array[i].id })
      .then(exists => {
        if (exists) {
          score == score++;
          return console.log(score);
        } else {
          return res.status(400).json({ msg: "wrong answer" });
        }
      });
    mongoose.deleteModel(subjectName);
  }
});

const UserDetails = mongoose.model(
  UsersCollection,
  new mongoose.Schema({
    email: String,
    password: String
  })
);

dynamicModel = subject => {
  var QuestionSchema = new mongoose.Schema({
    subject: String,
    level: Number,
    description: String,
    optionA: String,
    optionB: String,
    optionC: String,
    optionD: String,
    answer: Number
  });
  return mongoose.model(subject, QuestionSchema);
};
/**
 *@swagger
 * "/quiz/:subject/:level/mcq": {
 *   "post": {
 *     "description": "Upload Questions folowwing schema",
 *     "responses": {
 *       "200",
 *          "description": "Model Format include subject, level, description, option-a,b,c,d, answer",
 *          default:{
 *           "description": "Something is wrong"
 *              }
 *            }
 *          }
 *         }
 */
app.post("/quiz/:subject/question", access, (req, res) => {
  res.type("application/json");
  const questionBody = {
    subjectName: req.params.subject,
    level: req.body.level,
    description: req.body.description,
    optionA: req.body.optionA,
    optionB: req.body.optionB,
    optionC: req.body.optionC,
    optionD: req.body.optionD,
    answer: req.body.answer
  };
  if (
    !questionBody.subjectName.trim() ||
    (questionBody.level !== 1 &&
      questionBody.level !== 2 &&
      questionBody.level !== 3) ||
    !questionBody.description.trim() ||
    !questionBody.optionA.trim() ||
    !questionBody.optionB.trim() ||
    !questionBody.optionC.trim() ||
    !questionBody.optionD.trim() ||
    (questionBody.answer !== 1 &&
      questionBody.answer !== 2 &&
      questionBody.answer !== 3 &&
      questionBody.answer !== 4)
  ) {
    res.json({ msg: "Enter question using proper format" });
  } else {
    dynamicModel(questionBody.subjectName)
      .insertMany({
        subject: questionBody.subjectName,
        level: questionBody.level,
        description: questionBody.description,
        optionA: questionBody.optionA,
        optionB: questionBody.optionB,
        optionC: questionBody.optionC,
        optionD: questionBody.optionD,
        answer: questionBody.answer
      })
      .then(doc => res.send(doc[0]._id.toString()));
    mongoose.deleteModel(questionBody.subjectName);
  }
});
